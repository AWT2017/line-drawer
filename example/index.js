/*jslint browser: true */
/*globals ko, $ */

function ViewModel() {
    var self = this;
    self.line = ko.observable();

    self.line.subscribe(function () {
        alert(self.line());
    });

    self.clear = function () {
        self.line('');
    };
}

ko.applyBindings(new ViewModel());
